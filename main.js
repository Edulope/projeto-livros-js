adiciona = (e) => {
    e.preventDefault();
    if(e.target.nodeName === "BUTTON"){
        let text = document.getElementById("add-book");
        if(text[0].value !== ""){
                const Livro = {
                "book": {
                "name" : text[0].value,
                "author": text[1].value,
                }
              }
              text[0].value = "";
              text[1].value = "";
            fetch(`${url}/books`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                  },
                body: JSON.stringify(Livro),
            })
            .then(parseRequestToJSON)
            .then((livro) => {
                let tr = document.createElement("tr");
                for(let j = 0; j<5; j++){
                    let td  = document.createElement("td");
                    switch(j){
                        case(0):td.innerHTML = livro.id; break;
                        case(1):td.innerHTML = livro.name; break;
                        case(2):td.innerHTML = livro.author; break;
                        case(3):td.innerHTML = livro.created_at; break;
                        case(4):td.innerHTML = livro.updated_at; break;
                    }
                    tr.appendChild(td);
                }
                let excluir = document.createElement("span");
                excluir.className = "delete";
                excluir.id = livro.id;
                excluir.innerText = "excluir";
                tr.appendChild(excluir);
                livros.appendChild(tr);
            })
        }
    }
};



const url = "https://treinamento-api.herokuapp.com";

///FUNCAO DE DELETAR
function deletarLivroPeloID(id){
    fetch(`${url}/books/${id.innerHTML}`, {
      method: 'DELETE'
    })
    .catch(() => console.log("falha na exclusão"))
  }

remove = (e) => {
  if(e.target.className === 'delete'){
    deletarLivroPeloID(e.target.parentNode.firstChild);
    e.target.parentNode.remove();
}}

let livros = document.querySelector(".livros");
livros.addEventListener("click", remove);

let adicao = document.querySelector("#add-book");
adicao.addEventListener("click", (e) => adiciona(e));

///CARREGA O BANDO DE DADOS

const parseRequestToJSON = (requestResult) => {
    return requestResult.json();
}

fetch(url)
.then(parseRequestToJSON)
.then((responseAsJson) => {
    for(let i = 0; i<responseAsJson.length;i++){
        livro = responseAsJson[i];
        let tr = document.createElement("tr");
        for(let j = 0; j<5; j++){
            let td  = document.createElement("td");
            switch(j){
                case(0):td.innerHTML = responseAsJson[i].id; break;
                case(1):td.innerHTML = responseAsJson[i].name; break;
                case(2):td.innerHTML = responseAsJson[i].author; break;
                case(3):td.innerHTML = responseAsJson[i].created_at; break;
                case(4):td.innerHTML = responseAsJson[i].updated_at; break;
            }
            tr.appendChild(td);
        }
        let excluir = document.createElement("span");
        excluir.className = "delete";
        excluir.id = responseAsJson[i].id;
        excluir.innerText = "excluir";
        tr.appendChild(excluir);
        livros.appendChild(tr);
    }
});